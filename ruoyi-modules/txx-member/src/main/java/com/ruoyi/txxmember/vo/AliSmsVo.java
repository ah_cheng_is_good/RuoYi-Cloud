package com.ruoyi.txxmember.vo;

import lombok.Data;

/**
 * 阿里云短信变量
 */
@Data
public class AliSmsVo {

    /**
     * 验证码
     */
    private String code;
}
