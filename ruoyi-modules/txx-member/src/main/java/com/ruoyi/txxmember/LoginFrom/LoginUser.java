package com.ruoyi.txxmember.LoginFrom;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@ApiModel("登录表单")
public class LoginUser {
    /**
     * 用户名
     */
    @ApiModelProperty("用户名（不能为空，0-15个字符）")
    private String userName;



    /**
     * 密码
     */
    @ApiModelProperty("密码（不能为空，0-15个字符）")
    private String password;

    @NotBlank(message = "用户账号不能为空")
    @Size(min = 0, max = 15, message = "用户账号长度不能超过15个字符")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @NotBlank(message = "密码不能为空")
    @Size(min = 0, max = 15, message = "密码长度不能超过15个字符")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    @Override
    public String toString() {
        return "LoginUser{" +
                "userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
