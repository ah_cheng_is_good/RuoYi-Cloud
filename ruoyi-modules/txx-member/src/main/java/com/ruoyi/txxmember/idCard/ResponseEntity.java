package com.ruoyi.txxmember.idCard;

import lombok.Data;

@Data
public class ResponseEntity {

    private Integer error_code;
    private String reason;
    private Result result;
}