package com.ruoyi.txxmember.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.common.core.domain.Member;

public interface MemberMapper extends BaseMapper<Member> {
}
