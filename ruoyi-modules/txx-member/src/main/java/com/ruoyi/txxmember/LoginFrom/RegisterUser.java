package com.ruoyi.txxmember.LoginFrom;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@ApiModel("注册表单")
public class RegisterUser {

    /**
     * 用户名
     */
    @ApiModelProperty("用户名（不能为空，0-15个字符）")
    private String userName;



    /**
     * 密码
     */
    @ApiModelProperty("密码（不能为空，0-15个字符）")
    private String password;

    /**
     * 手机号
     */
    @ApiModelProperty("手机号（请输入正确的手机号）")
    private String phonenumber;

    /**
     * 验证码
     */
    @ApiModelProperty("验证码")
    private String code;

    @Override
    public String toString() {
        return "LoginUser{" +
                "username='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", phonenumber='" + phonenumber + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
    @NotBlank(message = "用户账号不能为空")
    @Size(min = 0, max = 15, message = "用户账号长度不能超过15个字符")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @NotBlank(message = "密码不能为空")
    @Size(min = 0, max = 15, message = "密码长度不能超过15个字符")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhonenumber() {
        return phonenumber;
    }



    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }
    @NotBlank(message = "验证码不能为空")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
