package com.ruoyi.txxmember.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.common.core.domain.Problem;
import com.ruoyi.txxmember.vo.ProblemVo;

public interface IProblemService extends IService<Problem> {
    void problemSubmit(ProblemVo problem,String loginId);
}
