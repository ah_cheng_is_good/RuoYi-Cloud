package com.ruoyi.txxmember;


import com.ruoyi.common.core.domain.Member;
import com.ruoyi.txxmember.mapper.MemberMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class TxxMemberApplicationTests {

    @Autowired
    private MemberMapper memberMapper;

    @Test
    void contextLoads() {
        List<Member> members = memberMapper.selectList(null);
        System.out.println(members);
    }

}
