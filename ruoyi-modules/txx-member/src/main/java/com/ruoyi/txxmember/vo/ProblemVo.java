package com.ruoyi.txxmember.vo;

import com.ruoyi.common.core.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class ProblemVo {

    /** 问题标题 */
    @ApiModelProperty("问题标题")
    private String problemTitle;


    /** 问题反馈联系方式 */
    @ApiModelProperty("问题反馈联系方式")
    private String problemPhone;

    /** 问题证据图片数组方式存图片id[1,2,3] */
    @ApiModelProperty("问题证据图片数组方式存图片id")
    private List<String> problemPhoto;
}
