package com.ruoyi.txxmember.idCard;

import lombok.Data;

import java.util.Date;

@Data
public class IdCardInfor {

    private String province;
    private String city;
    private String district;
    private String area;
    private String sex;
    private Date birthday;

}