package com.ruoyi.txxmember.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.domain.Problem;
import com.ruoyi.common.core.exception.ServiceException;
import com.ruoyi.common.core.utils.PhoneFormatCheckUtils;
import com.ruoyi.txxmember.mapper.ProblemMapper;
import com.ruoyi.txxmember.service.IProblemService;
import com.ruoyi.txxmember.vo.ProblemVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProblemServiceImpl extends ServiceImpl<ProblemMapper, Problem> implements IProblemService {
    @Override
    public void problemSubmit(ProblemVo problem,String loginId) {

        boolean phoneLegal = PhoneFormatCheckUtils.isChinaPhoneLegal(problem.getProblemPhone());
        if(!phoneLegal){
            throw new ServiceException("手机号格式不正确，请重新输入");
        }
        Problem problem1 = new Problem();
        BeanUtils.copyProperties(problem,problem1);
        List<String> problemPhoto = problem.getProblemPhoto();
        String photoIds = problemPhoto.toString();
        problem1.setProblemPhoto(photoIds);
        problem1.setMemberId(Long.valueOf(loginId));
        this.save(problem1);
    }
}
