package com.ruoyi.txxmember.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.common.core.domain.Problem;

import java.util.List;

/**
 * 会员问题管理Mapper接口
 * 
 * @author lzc
 * @date 2021-09-16
 */
public interface ProblemMapper extends BaseMapper<Problem>
{
    /**
     * 查询会员问题管理
     * 
     * @param id 会员问题管理主键
     * @return 会员问题管理
     */
    public Problem selectProblemById(Long id);

    /**
     * 查询会员问题管理列表
     * 
     * @param problem 会员问题管理
     * @return 会员问题管理集合
     */
    public List<Problem> selectProblemList(Problem problem);

    /**
     * 新增会员问题管理
     * 
     * @param problem 会员问题管理
     * @return 结果
     */
    public int insertProblem(Problem problem);

    /**
     * 修改会员问题管理
     * 
     * @param problem 会员问题管理
     * @return 结果
     */
    public int updateProblem(Problem problem);

    /**
     * 删除会员问题管理
     * 
     * @param id 会员问题管理主键
     * @return 结果
     */
    public int deleteProblemById(Long id);

    /**
     * 批量删除会员问题管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProblemByIds(Long[] ids);
}
