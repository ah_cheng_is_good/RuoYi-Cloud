package com.ruoyi.txxmember.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 身份证验证信息
 */
@Data
public class IdCardVo {

    /**
     * 身份证号
     */
    @ApiModelProperty("身份证号")
    private String cardNo;

    /**
     * 姓名
     */
    @ApiModelProperty("姓名")
    private String realName;

    /**
     * 正面身份证
     */
    @ApiModelProperty("正面身份证")
    private Long positive;

    /**
     * 反面身份证
     */
    @ApiModelProperty("反面身份证")
    private Long reverse;
}
