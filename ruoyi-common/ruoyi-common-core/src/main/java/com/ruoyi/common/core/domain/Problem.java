package com.ruoyi.common.core.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 会员问题管理对象 member_problem
 * 
 * @author lzc
 * @date 2021-09-16
 */
@TableName("member_problem")
public class Problem extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    @TableId(type= IdType.AUTO)
    private Long id;

    /** 问题标题 */
    @Excel(name = "问题标题")
    private String problemTitle;

    /** 问题回答内容 */
    @Excel(name = "问题回答内容")
    private String problemAnswer;

    /** 问题反馈联系方式 */
    @Excel(name = "问题反馈联系方式")
    private String problemPhone;

    /** 问题证据图片数组方式存图片id[1,2,3] */
    @Excel(name = "问题证据图片数组方式存图片id[1,2,3]")
    private String problemPhoto;

    /** 是否是常见问题(0代表不是1代表是) */
    @Excel(name = "是否是常见问题(0代表不是1代表是)")
    private String isMany;

    /** 对应会员id */
    @Excel(name = "对应会员id")
    private Long memberId;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProblemTitle(String problemTitle) 
    {
        this.problemTitle = problemTitle;
    }

    public String getProblemTitle() 
    {
        return problemTitle;
    }
    public void setProblemAnswer(String problemAnswer) 
    {
        this.problemAnswer = problemAnswer;
    }

    public String getProblemAnswer() 
    {
        return problemAnswer;
    }
    public void setProblemPhone(String problemPhone) 
    {
        this.problemPhone = problemPhone;
    }

    public String getProblemPhone() 
    {
        return problemPhone;
    }
    public void setProblemPhoto(String problemPhoto) 
    {
        this.problemPhoto = problemPhoto;
    }

    public String getProblemPhoto() 
    {
        return problemPhoto;
    }
    public void setIsMany(String isMany) 
    {
        this.isMany = isMany;
    }

    public String getIsMany() 
    {
        return isMany;
    }
    public void setMemberId(Long memberId) 
    {
        this.memberId = memberId;
    }

    public Long getMemberId() 
    {
        return memberId;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("problemTitle", getProblemTitle())
            .append("problemAnswer", getProblemAnswer())
            .append("problemPhone", getProblemPhone())
            .append("problemPhoto", getProblemPhoto())
            .append("isMany", getIsMany())
            .append("memberId", getMemberId())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
