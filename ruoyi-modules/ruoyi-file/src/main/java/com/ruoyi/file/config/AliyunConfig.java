package com.ruoyi.file.config;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClient;
import com.ruoyi.common.core.constant.AliConstant;
import lombok.Data;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author 巅峰小词典
 * @description
 * @date 2021/5/20
 * @project springboot_oss
 */
@Configuration
@PropertySource(value = {"classpath:application-oss.properties"})
@Data
public class AliyunConfig {


    @Bean
    public OSS oSSClient() {
        return new OSSClient(AliConstant.ENDPOINT, AliConstant.ACCESS_KEYID, AliConstant.ACCESS_KEY_SECRET);
    }

}