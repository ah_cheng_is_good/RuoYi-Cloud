package com.ruoyi.common.core.controller;

import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BaseControllers {


    @Autowired
    private HttpServletRequest request;

    @Autowired
    private HttpServletResponse response;

    /**
     * 获取当前登录id
     * @return
     */
    public String getLoginId(){
        String header = request.getHeader(SecurityConstants.DETAILS_USER_ID);
        if(StringUtils.isEmpty(header)){
            return null;
        }
        return header;
    }
}
