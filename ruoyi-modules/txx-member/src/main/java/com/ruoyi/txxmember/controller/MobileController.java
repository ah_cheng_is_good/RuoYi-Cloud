package com.ruoyi.txxmember.controller;

import com.ruoyi.common.core.controller.BaseControllers;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;

import com.ruoyi.txxmember.LoginFrom.LoginUser;
import com.ruoyi.txxmember.LoginFrom.RegisterUser;
import com.ruoyi.txxmember.service.IMemberService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("mobile")
@Api(tags = "登录注册相关")
public class MobileController extends BaseControllers {

    @Autowired
    private IMemberService memberService;

    /**
     * 用户注册
     * @param registerUser
     * @return
     */
    @PostMapping("register")
    @ApiOperation("注册")
    public AjaxResult register(@Validated @RequestBody RegisterUser registerUser){
        memberService.register(registerUser);
        return AjaxResult.success("注册成功");
    }


    /**
     * 发送验证码
     * @param phonenumber
     * @return
     */
    @PostMapping("sendsms")
    @ApiOperation("发送验证码")
    public AjaxResult sendsms(String phonenumber){
        memberService.sendsms(phonenumber);
        return AjaxResult.success("验证码已发送");
    }

    /**
     * 登录
     * @param loginUser
     * @return
     */
    @PostMapping("login")
    @ApiOperation("登录")
    public AjaxResult login(@Validated @RequestBody LoginUser loginUser){
        return memberService.login(loginUser);
    }

    /**
     * 退出
     * @param
     * @return
     */
    @PostMapping("loginout")
    @ApiOperation("登录退出")
    public AjaxResult loginout(){
        String loginId = this.getLoginId();
        return memberService.loginout(loginId);
    }

}
