package com.ruoyi.txxmember.idCard;

import lombok.Data;

@Data
public class Result {

    private String realname;
    private String idcard;
    private Boolean isok;
    private IdCardInfor IdCardInfor;

}