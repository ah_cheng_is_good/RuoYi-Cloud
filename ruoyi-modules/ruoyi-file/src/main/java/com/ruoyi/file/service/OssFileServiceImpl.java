package com.ruoyi.file.service;

import com.aliyun.oss.OSS;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.core.constant.AliConstant;
import com.ruoyi.common.core.domain.OssResources;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.exception.ServiceException;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.file.config.AliyunConfig;
import com.ruoyi.system.api.RemoteUserService;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.ByteArrayInputStream;


/**
 * oss文件上传接口
 *
 * @author ruoyi
 */
@Service
public class OssFileServiceImpl implements ISysFileService{
    @Resource
    private OSS ossClient;
    @Resource
    private RemoteUserService remoteUserService;
    // 允许上传的格式
    private static final String[] IMAGE_TYPE = new String[]{".bmp", ".jpg", ".jpeg", ".gif", ".png"};

    @Override
    public OssResources uploadFile(MultipartFile uploadFile) throws Exception {
        // 校验图片格式
        boolean isLegal = false;
        for (String type : IMAGE_TYPE) {
            if (StringUtils.endsWithIgnoreCase(uploadFile.getOriginalFilename(), type)) {
                isLegal = true;
                break;
            }
        }
        // 封装Result对象，并且将文件的byte数组放置到result对象中
        if (!isLegal) {
          throw new ServiceException("文件上传的格式有误，仅支持{\".bmp\", \".jpg\", \".jpeg\", \".gif\", \".png\"}格式");
        }
        // 文件新路径
        String fileName = uploadFile.getOriginalFilename();
        String filePath = getFilePath(fileName);
        // 上传到阿里云
        try {
            ossClient.putObject(AliConstant.BUCKETNAME, filePath, new ByteArrayInputStream(uploadFile.getBytes()));
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("文件上传失败");
        }
        // 文件路径需要保存到数据库
        String fileUrl=AliConstant.URLPREFIX+filePath;
        OssResources ossResources = new OssResources();
        ossResources.setLink(fileUrl);
        ossResources.setName(fileName);
        //保存到数据库
        R<OssResources> resourcesR = remoteUserService.into(ossResources);
        if(resourcesR.getCode()!=200){
            throw new ServiceException("调用系统文件操作失败");
        }
        OssResources resources = resourcesR.getData();
        return resources;
    }


    /**
     * 生成路径以及文件名
     *
     * @param sourceFileName
     * @return
     */
    private String getFilePath(String sourceFileName) {
        DateTime dateTime = new DateTime();
        return "images/" + dateTime.toString("yyyy") + "/" + dateTime.toString("MM") + "/"
                + dateTime.toString("dd") + "/" + System.currentTimeMillis()
                + RandomUtils.nextInt(100, 9999) + "."
                + StringUtils.substringAfterLast(sourceFileName, ".");
    }

//    /**
//     * 查看文件列表
//     *
//     * @return
//     */
//    public List<OSSObjectSummary> list() {
//        // 设置最大个数。
//        final int maxKeys = 200;
//        // 列举文件。
//        ObjectListing objectListing = ossClient.listObjects(new ListObjectsRequest(aliyunConfig.getBucketName()).withMaxKeys(maxKeys));
//        List<OSSObjectSummary> sums = objectListing.getObjectSummaries();
//        return sums;
//    }
//
//    /**
//     * 删除文件
//     *
//     * @param objectName
//     * @return
//     */
//    public FileUploadResult delete(String objectName) {
//        // 根据BucketName,objectName删除文件
//        ossClient.deleteObject(aliyunConfig.getBucketName(), objectName);
//        FileUploadResult fileUploadResult = new FileUploadResult();
//        fileUploadResult.setName(objectName);
//        fileUploadResult.setStatus("removed");
//        fileUploadResult.setResponse("success");
//        return fileUploadResult;
//    }
//
//    /**
//     * 下载文件
//     *
//     * @param os
//     * @param objectName
//     * @throws IOException
//     */
//    public void exportOssFile(OutputStream os, String objectName) throws IOException {
//        // ossObject包含文件所在的存储空间名称、文件名称、文件元信息以及一个输入流。
//        OSSObject ossObject = ossClient.getObject(aliyunConfig.getBucketName(), objectName);
//        // 读取文件内容。
//        BufferedInputStream in = new BufferedInputStream(ossObject.getObjectContent());
//        BufferedOutputStream out = new BufferedOutputStream(os);
//        byte[] buffer = new byte[1024];
//        int lenght = 0;
//        while ((lenght = in.read(buffer)) != -1) {
//            out.write(buffer, 0, lenght);
//        }
//        if (out != null) {
//            out.flush();
//            out.close();
//        }
//        if (in != null) {
//            in.close();
//        }
//    }
}
