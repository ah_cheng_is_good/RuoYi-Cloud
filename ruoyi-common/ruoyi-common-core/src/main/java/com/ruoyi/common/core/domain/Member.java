package com.ruoyi.common.core.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 会员信息对象 member_member
 * 
 * @author lzc
 * @date 2021-09-14
 */
@TableName("member_member")
@Data
public class Member extends BaseEntity
{
    private static final long serialVersionUID = 1L;


    /**
     * 登录时间
     */
    @TableField(exist = false)
    private Long loginTime;

    /**
     * 过期时间
     */
    @TableField(exist = false)
    private Long expireTime;

    /**
     * 用户唯一标识 token
     */
    @TableField(exist = false)
    private String token;

    /** 用户ID */
    @TableId(type = IdType.AUTO)
    private Long id;

    /** 用户账号 */
    @Excel(name = "用户账号")
    private String userName;

    /** 用户昵称 */
    @Excel(name = "用户昵称")
    private String nickName;
    /**
     * 身份证号
     */
    private String cardNo;

    /**
     * 姓名
     */
    private String realName;

    /**
     * 正面身份证
     */
    private Long positive;

    /**
     * 反面身份证
     */
    private Long reverse;

    /** 用户邮箱 */
    @Excel(name = "用户邮箱")
    private String email;
    /**
     * 用户id编号
     */
    private String userCode;

    /** 手机号码 */
    @Excel(name = "手机号码")
    private String phonenumber;

    /** 用户性别（0男 1女 2未知） */
    @Excel(name = "用户性别", readConverterExp = "0=男,1=女,2=未知")
    private String sex;

    /** 头像地址 */
    @Excel(name = "头像地址")
    private String avatar;

    /** 密码 */
    @Excel(name = "密码")
    private String password;

    /**
     * 交易密码 默认和登录密码一样
     */
    private String tradingPassword;

    /** 帐号状态（0正常 1停用） */
    @Excel(name = "帐号状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 最后登录IP */
    @Excel(name = "最后登录IP")
    private String loginIp;

    /** 最后登录时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后登录时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date loginDate;

    /**
     * 登录IP地址
     */
    @TableField(exist = false)
    private String ipaddr;
}
