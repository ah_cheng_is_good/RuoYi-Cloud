package com.ruoyi.txxmember.controller;


import com.ruoyi.common.core.controller.BaseControllers;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.domain.Member;
import com.ruoyi.txxmember.service.IMemberService;
import com.ruoyi.txxmember.service.IProblemService;
import com.ruoyi.txxmember.vo.IdCardVo;
import com.ruoyi.txxmember.vo.ProblemVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/member")
@Api(tags = "前台会员相关")
public class MenberController extends BaseControllers {


    @Autowired
    private IMemberService memberService;
    @Autowired
    private IProblemService problemService;

    @ApiOperation("所有用户信息")
    @GetMapping("/allMemberInfo")
    public AjaxResult memberList(){
        List<Member> memberList = memberService.list();
        return AjaxResult.success(memberList);
    }


    /**
     * 修改登录密码
     * @return
     */
    @ApiOperation("修改登录密码")
    @PostMapping("/updateLoginPassword")
    public AjaxResult updateLoginPassword(String oldPwd, String newPwd){
        String loginId = this.getLoginId();
        memberService.updateLoginPassword(oldPwd,newPwd,loginId);
        return AjaxResult.success("修改登录密码成功");
    }


    /**
     * 修改交易密码
     * @return
     */
    @ApiOperation("修改交易密码")
    @PostMapping("/updateTradingPassword")
    public AjaxResult updateTradingPassword(String oldTradingPwd, String newTradingPwd){
        String loginId = this.getLoginId();
        memberService.updateTradingPassword(oldTradingPwd,newTradingPwd,loginId);
        return AjaxResult.success("修改交易密码成功");
    }

    /**
     * 忘记交易密码
     * @return
     */
    @ApiOperation("忘记交易密码")
    @PostMapping("/forgetTradingPassword")
    public AjaxResult forgetTradingPassword(String phoneNum,String newPwd,String code){
        String loginId = this.getLoginId();
        memberService.forgetTradingPassword(phoneNum,newPwd,code,loginId);
        return AjaxResult.success("交易密码重置成功");
    }

    /**
     * 个人中心用户信息
     * @return
     */
    @ApiOperation("个人中心用户信息")
    @GetMapping("memberInfo")
    public AjaxResult memberInfo(){
        String loginId = this.getLoginId();
        Member member=memberService.memberInfo(loginId);
        return AjaxResult.success(member);
    }

    /**
     * 问题和反馈提交
     * @return
     */
    @ApiOperation("问题和反馈提交")
    @PostMapping("problemSubmit")
    public AjaxResult problemSubmit(@RequestBody ProblemVo problem){
        String loginId = this.getLoginId();
        problemService.problemSubmit(problem,loginId);
        return AjaxResult.success();
    }

    /**
     * 身份证实名认证
     * @return
     */
    @ApiOperation("身份证实名认证")
    @PostMapping("idCardValidation")
    public AjaxResult idCardValidation(@RequestBody IdCardVo idCardVo){
        memberService.idCardValidation(idCardVo);
        return AjaxResult.success();
    }




}
