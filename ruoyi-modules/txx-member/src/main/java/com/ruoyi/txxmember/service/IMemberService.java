package com.ruoyi.txxmember.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.txxmember.LoginFrom.LoginUser;
import com.ruoyi.txxmember.LoginFrom.RegisterUser;
import com.ruoyi.common.core.domain.Member;
import com.ruoyi.txxmember.vo.IdCardVo;

public interface IMemberService extends IService<Member> {

    void register(RegisterUser registerUser);

    void sendsms(String phonenumber);

    AjaxResult login(LoginUser loginUser);

    void updateLoginPassword(String oldPwd, String newPwd,String loginId);

    void updateTradingPassword(String oldTradingPwd, String newTradingPwd, String loginId);

    void forgetTradingPassword(String phoneNum, String newPwd,String code, String loginId);

    Member memberInfo(String loginId);

    AjaxResult loginout(String loginId);

    void idCardValidation(IdCardVo idCardVo);
}
